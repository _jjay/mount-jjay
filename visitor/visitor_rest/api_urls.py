from django.urls import path
from .api_views import api_create_user, api_login



urlpatterns = [

    path("signup/", api_create_user, name="signup"),
    path("login/", api_login, name="login")
]
