from django.http import JsonResponse
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
import json
import pika
from django.contrib.auth.models import User
from django.core.serializers import serialize
from django.contrib.auth import login, authenticate, logout


from common.json import ModelEncoder

class UserEncoder(ModelEncoder):
    model = User,
    properties = [
        "username",
        "first_name",
        "last_name",
        "email",
        "password",
    ]

def send_visitor_info(visitor):
    parameters = pika.ConnectionParameters(host="rabbitmq")
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare(queue="visitors")
    message = json.dumps(visitor, cls=UserEncoder)
    channel.basic_publish(
        exchange="",
        routing_key="visitors",
        body=message,
    )
    connection.close()




def create_user(json_content):
    try:
        content = json.loads(json_content)
    except json.JSONDecodeError:
        return 400, {"message": "Bad JSON"}, None

    required_properties = [
        "username",
        "email",
        "password",
        "first_name",
        "last_name",
    ]
    missing_properties = []
    for required_property in required_properties:
        if (
            required_property not in content
            or len(content[required_property]) == 0
        ):
            missing_properties.append(required_property)
    if missing_properties:
        response_content = {
            "message": "missing properties",
            "properties": missing_properties,
        }
        return 400, response_content, None

    try:
        account = User.objects.create_user(
            username=content["username"],
            email=content["email"],
            password=content["password"],
            first_name=content["first_name"],
            last_name=content["last_name"]
        )
        # account_data = serialize('json', [account], cls=UserEncoder)
        # account_data = JsonResponse(account, encoder=UserEncoder, safe=False)
        # send_visitor_info(account_data)
        return 200, account, account
    except IntegrityError as e:
        return 409, {"message": str(e)}, None
    except ValueError as e:
        return 400, {"message": str(e)}, None


def api_create_user(request):
    status_code, response_content, _ = create_user(request.body)
    if status_code >= 200 and status_code < 300:
        send_visitor_info(response_content)
    response = JsonResponse(
        response_content,
        encoder=UserEncoder,
        safe=False,
    )
    response.status_code = status_code
    return response


def api_login(request):
    if request.method == "POST":
        content = json.loads(request.body)
        username = content["username"]
        password = content["password"]

        user = authenticate(
            request,
            username=username,
            password=password,
        )
        if user is not None:
            login(request, user)
            return JsonResponse (
                {"message:": "You've been logged in!"}
            )
