from django.apps import AppConfig


class VisitorRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "visitor_rest"
