from django.shortcuts import render

def game_main(request):
    return render(request, "game/main.html")

def minigames(request):
    return render(request, "game/minigames.html")

def ash(request):
    return render(request, "game/ash.html")

def jackie(request):
    return render(request, "game/jackie.html")

def jordan(request):
    return render(request, "game/jordan.html")

def young(request):
    return render(request, "game/young.html")
