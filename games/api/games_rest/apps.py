from django.apps import AppConfig


class GamesRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "games_rest"
