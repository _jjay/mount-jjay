from django.urls import path
from games_rest.views import game_main, minigames, ash, jackie, jordan, young

urlpatterns = [
    path("", game_main, name="game_main"),
    path("minigames/", minigames, name="minigames"),
    path("ash/", ash, name="ash"),
    path("jackie/", jackie, name="jackie"),
    path("jordan", jordan, name="jordan"),
    path("young", young, name="young"),
]
