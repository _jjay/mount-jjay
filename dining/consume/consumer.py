import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "dining_project.settings")
django.setup()

from django.contrib.auth.models import User


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq")
        )
        channel = connection.channel()
        channel.queue_declare(queue="visitors")

        def create_user(ch, method, properties, body):
            data = json.loads(body)
            User.objects.create_user(
                email=data["email"],
                first_name=data["first_name"],
                last_name=data["last_name"],
                username=data["username"],
                password=data["password"]

            )
            print("User created!")


        channel.basic_consume(
            queue="visitors",
            on_message_callback=create_user,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
