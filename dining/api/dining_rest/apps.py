from django.apps import AppConfig


class DiningRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "dining_rest"
