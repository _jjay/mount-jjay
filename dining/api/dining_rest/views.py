from django.shortcuts import render
from .models import FoodOrder
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from django.http import JsonResponse

class FoodOrderEncoder(ModelEncoder):
    model = FoodOrder
    properties = [
        "visitor_name",
        "room_number",
        "food_items",
        "time",
        "date",
        "status",
        "notes",
        "phone_number",
        "id",
    ]



@require_http_methods(["GET", "POST"])
def food_order(request):
    if request.method == "POST":
        content = json.loads(request.body)
        food_list = content["food_items"]
        content["food_items"] = json.dumps(food_list)
        food_order = FoodOrder.objects.create(**content)
        return JsonResponse(
            {"order": food_order},
            encoder=FoodOrderEncoder,
        )
    else:
        orders = FoodOrder.objects.all()
        return JsonResponse(
            {"orders": orders},
            encoder=FoodOrderEncoder,
        )


@require_http_methods(["PUT"])
def cancel_order(request, id):
    order = FoodOrder.objects.get(id=id)
    order.cancel()
    return JsonResponse(
        order,
        encoder=FoodOrderEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def start_order(request, id):
    order = FoodOrder.objects.get(id=id)
    order.in_progress()
    return JsonResponse(
        order,
        encoder=FoodOrderEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def send_order(request, id):
    order = FoodOrder.objects.get(id=id)
    order.out_for_delivery()
    return JsonResponse(
        order,
        encoder=FoodOrderEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def deliver_order(request, id):
    order = FoodOrder.objects.get(id=id)
    order.complete()
    return JsonResponse(
        order,
        encoder=FoodOrderEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def process_orders(request):
    all_orders = FoodOrder.objects.all()
    some_orders = all_orders.exclude(status="CANCELED")
    orders = some_orders.exclude(status="DELIVERED")
    return JsonResponse(
            {"orders": orders},
            encoder=FoodOrderEncoder,
        )
