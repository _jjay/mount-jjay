from django.urls import path

from .views import food_order, cancel_order, start_order, send_order, deliver_order, process_orders

urlpatterns = [
    path("order/", food_order, name="food_order"),
    path("cancel/<int:id>/", cancel_order, name="cancel_order"),
    path("start/<int:id>/", start_order, name="start_order"),
    path("send/<int:id>/", send_order, name="send_order"),
    path("deliver/<int:id>/", deliver_order, name="deliver_order"),
    path("orders/", process_orders, name="process_orders"),
]
