from django.contrib import admin
from .models import FoodOrder


@admin.register(FoodOrder)
class FoodOrderAdmin(admin.ModelAdmin):
    pass
