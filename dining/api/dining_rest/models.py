from django.db import models

# Create your models here.
class FoodOrder(models.Model):
    visitor_name = models.CharField(max_length=100)
    room_number = models.IntegerField()
    food_items = models.JSONField()
    time = models.TimeField()
    date = models.DateField()
    status = models.CharField(max_length=100, default="OREDERED")
    notes = models.TextField()
    phone_number = models.CharField(max_length=10)

    def cancel(self):
        self.status = "CANCELED"
        self.save()

    def in_progress(self):
        self.status = "IN PROGRESS"
        self.save()

    def out_for_delivery(self):
        self.status = "OUT FOR DELIVERY"
        self.save()

    def complete(self):
        self.status = "DELIVERED"
        self.save()

    class Meta():
        ordering = ["date", "time", "room_number"]
