from django.urls import path
from .views import stay_list_reservations

urlpatterns = [
    path("stay/", stay_list_reservations, name="stay_list_reservations"),
]
