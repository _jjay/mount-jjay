from django.shortcuts import render, redirect
from .models import Stay
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class StayEncoder(ModelEncoder):
    model = Stay
    properties = [
        "start_date",
        "end_date",
        "room_type",
        "room_names",
        "id",
    ]

@require_http_methods(["GET", "POST"])
def stay_list_reservations(request):
    if request.method == "GET":
        reservations = Stay.objects.all()
        return JsonResponse(
            {"reservations" : reservations},
            encoder=StayEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            reservations = Stay.objects.create(**content)
            return JsonResponse(
                reservations,
                encoder=StayEncoder,
                safe=False,
            )
        except:
            response = JsonResponse({"message": "Could not create reservation."})
            response.status_code = 400
            return response
