from django.db import models
from django.urls import reverse


# Create your models here.
room_names = (
    ("lake_blap", "Lake Blap"),
    ("yosemite", "Yosemite"),
    ("kilchis_river", "Kilchis River"),
    ("sequoia", "Sequoia"),
    ("big_basin", "Big Basin")
)

room_type = (
    ("true", "Romance"),
    ("false", "Childrens"),
    ("null", "None"),
)

class Stay(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    room_type = models.CharField(
        max_length=50,
        choices=room_type,
    )
    room_names = models.CharField(
        max_length=50,
        choices=room_names,
    )

    def get_api_url(self):
        return reverse("stay_list_reservations", kwargs={"pk": self.pk})

    def __str__(self):
        return self.room_names
