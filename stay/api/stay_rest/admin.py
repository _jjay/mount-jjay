from django.contrib import admin
from .models import Stay

# Register your models here.
@admin.register(Stay)
class StayAdmin(admin.ModelAdmin):
    pass
