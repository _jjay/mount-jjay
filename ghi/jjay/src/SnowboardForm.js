import React, {useEffect, useState} from 'react';
import './Snowboard.css';

function SnowboradForm () {
    const [name, setName] = useState('')
    const [date, setDate] = useState('')
    const [email, setEmail] = useState('')
    const [bootSize, setBootSize] = useState('')
    const [boardLength, setBoardLength] = useState('')
    const [equipmentRental, setEquipmentRental] = useState('')

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value)
    }
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value)
    }
    const handleBootSizeChange = (event) => {
        const value = event.target.value;
        setBootSize(value)
    }
    const handleBoardLengthChange = (event) => {
        const value = event.target.value;
        setBoardLength(value)
    }
    const handleEquipmentRentalChange = (event) => {
        const value = event.target.value;
        setEquipmentRental(value)
    }

    const handleSubmit = async (event) => {
        event.preventDafault()

        const data = {}

        data.name = name
        data.date = date
        data.email = email
        data.boot_size = bootSize
        data.board_length = boardLength
        data.equipment_rental = equipmentRental

        const snowboardUrl = 'http://localhost:8030/api/snowboard/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const snowboardResponse = await fetch(snowboardUrl, fetchConfig);
        if (snowboardResponse.ok) {
            const newSnowboard = await snowboardResponse.json();

            setName('');
            setDate('');
            setEmail('');
            setBootSize('');
            setBoardLength('');
            setEquipmentRental('');
        }
    }

    const boot_size_choices = [
        "Y1", "Y2", "Y3",
        "Y4", "Y5", "Y6",
        "6", "6.5",
        "7", "7.5",
        "8", "8.5",
        "9", "9.5",
        "10", "10.5",
        "11", "11.5",
        "12", "12.5",
        "13", "13.5",
    ]

    return (
        <div className="Snowboard">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">

                        <div className="form-board">

                        <form className="w-full max-w-sm" onSubmit={handleSubmit} id="create-snowboard-form">
                        <h1 className="mb-4 text-3xl font-extrabold text-gray-900 dark:text-white md:text-5xl lg:text-6xl"><span className="heyo-1 text-transparent bg-clip-text bg-gradient-to-r to-emerald-600 from-sky-400">Time to Snowboard</span></h1>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                        <label className="heyo-2 block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-first-name">
                            First & Last Name
                        </label>
                            <input onChange={handleNameChange} value={name} required type="text" name="name" id="name" className="form-control" />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="heyo-2 block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-date-time">
                                Date Time
                            </label>
                            <input onChange={handleDateChange} value={date} placeholder="date" required type="datetime-local" name="date" id="date" className="form-control" step="60" />
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <label className="heyo-2 block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" htmlFor="grid-email">
                                Email
                            </label>
                            <input onChange={handleEmailChange} value={email} required type="email" name="email" id="email" className="form-control" />
                            <p className="heyo-2 text-red-500 text-xs italic">Please fill out this field.</p>

                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <select onChange={handleBootSizeChange} value={bootSize} required id="boot_size" name="boot_size" className="form-select">
                                <option value="">Boot Size</option>
                                {boot_size_choices.map((size) => (
                                    <option key={size} value={size}>{size}</option>
                                ))}
                            </select>
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <select onChange={handleBoardLengthChange} value={boardLength} id="board_length" name="board_length" className="form-select">
                                <option value="">Board Length</option>
                                <option value="135cm">135cm</option>
                                <option value="138cm">138cm</option>
                                <option value="142cm">142cm</option>
                                <option value="146cm">146cm</option>
                                <option value="151cm">151cm</option>
                                <option value="155cm">155cm</option>
                                <option value="157cm">157cm</option>
                                <option value="159cm">159cm</option>
                                <option value="161cm">161cm</option>
                                <option value="166cm">166cm</option>
                                <option value="172cm">172cm</option>
                            </select>
                        </div>
                        <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                            <select onChange={handleEquipmentRentalChange} value={equipmentRental} required name="equipment_rental" id="equipment_rental" className="form-control">
                                <option value="">Need Rental?</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                        <button className="heyo-btn bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Send It!</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default SnowboradForm;
