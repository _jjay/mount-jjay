import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import '../Site.css';


function ReservationList() {

  const [reservations, setResevation] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8040/api/stay/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setResevation(data.reservations)
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  const romance = 'https://img.icons8.com/stickers/2x/novel.png'
  const children = 'https://img.icons8.com/external-wanicon-flat-wanicon/2x/external-toys-supermarket-wanicon-flat-wanicon.png'

  return (
    <>
    <div className='container'>
      <div className='hero-container'>
        <video className='video-stay' src='/videos/stay_video.mp4' autoPlay loop muted />
        <h1>MOUNT JJAY</h1>
        <p>We're happy to have you!</p>
        <div className="d-grid gap-2 mt-3 d-sm-flex justify-content-sm-center lead mb-3">
          <Link to="/stay/new" className="btn bg-white btn-lg px-4 gap-3">Make New Reservation</Link>
        </div>
      </div>
      <table className="table-stay">
        <thead className="thead-dark td-stay">
          <tr>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Room Type</th>
            <th>Room Names</th>
          </tr>
        </thead>
        <tbody className='td-stay'>
          {reservations.map(reservation => {
            return (
              <tr key={reservation.id}>
                <td className='td-stay'>{reservation.start_date}</td>
                <td className='td-stay'>{reservation.end_date}</td>
                <td className='td-stay'>
                  {reservation.room_type === "true" ? <img style={{ width: 30, height: 30 }} src={romance}></img> : reservation.room_type === "false" ? <img style={{ width: 30, height: 30 }} src={children}></img> : "None"}
                </td>
                <td className='td-stay'>{reservation.room_names}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    </>
  );
}
export default ReservationList;
