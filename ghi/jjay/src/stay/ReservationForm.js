import React, { useState, useEffect } from 'react';
import '../Site.css';

function ReservationForm() {


    const [startDate, setStartDate] = useState('');
    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value)
    }

    const [endDate, setEndDate] = useState('');
    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value)
    }

    const [roomType, setRoomType] = useState('');
    const handleRoomTypeChange = (event) => {
        const value = event.target.value;
        setRoomType(value)
    }

    const [roomName, setRoomName] = useState('');
    const handleRoomNameChange = (event) => {
        const value = event.target.value;
        setRoomName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.start_date = startDate;
        data.end_date = endDate;
        data.room_type = roomType;
        data.room_names = roomName;

        const reservationUrl = 'http://localhost:8040/api/stay/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(reservationUrl, fetchConfig);
        if (response.ok) {
            const newReservation = await response.json();

            setStartDate('');
            setEndDate('');
            setRoomType('');
            setRoomName('');
        }
    }

    return (
        <div className='container'>
            <div className="grid grid-cols-6 gap-4">
                <div className="col-start-2 col-span-4 center">
                    <img
                        src="/images/top.jpeg"
                        className="shadow p-4 mt-4 rounded mb-0" />
                </div>
                {/* <div className="col-start-1 col-end-3">
                    <img
                        src="/images/firepit.jpeg"
                        className="shadow p-4 mt-3.5 rounded mb-4" />
                </div> */}
                <div className="col-start-3 col-span-2">
                    <div className="row">
                        <div className="shadow p-4 mt-2">
                            <h1>Reserve A room</h1>
                            <form onSubmit={handleSubmit} id="create-presentation-form">
                                <div className="form-floating mb-3">
                                    <input onChange={handleStartDateChange} value={startDate} placeholder="start date" required type="date" name="start date"
                                        id="start date" className="form-control" />
                                    <label htmlFor="name">Start Date</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleEndDateChange} value={endDate} placeholder="end date" required type="date" name="end date"
                                        id="end date" className="form-control" />
                                    <label htmlFor="style">End Date</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <select onChange={handleRoomTypeChange} value={roomType} name="room type" id="room type" className="form-select" required>
                                        <option value="null">Choose an Adventure Type (optional)</option>
                                        <option value={"true"}>Romance</option>
                                        <option value={"false"}>Children</option>
                                        <option value={"null"}>None</option>
                                    </select>
                                </div>
                                <div className="form-floating mb-3">
                                    <select onChange={handleRoomNameChange} value={roomName} name="room name" id="room name" className="form-select" required>
                                        <option value="">Choose a Room</option>
                                        <option value={"Lake Blap"}>Lake Blap</option>
                                        <option value={"Yosemite"}>Yosemite</option>
                                        <option value={"Kilchis River"}>Kilchis River</option>
                                        <option value={"Sequoia"}>Sequoia</option>
                                        <option value={"Big Basin"}>Big Basin</option>
                                    </select>
                                </div>
                                <button className="btn btn-primary">Create Reservation</button>
                            </form>
                        </div>

                    </div>
                </div>
                {/* <div className="cole-end-7 col-span-2 ">
                    <img
                        src="/images/mtnbckgrnd.jpeg"
                        className="shadow p-4 mt-3.5 rounded mb-4" />
                </div> */}
                <div className="col-start-1 col-end-7 ">
                    <img
                        src="/images/lower.jpeg"
                        className="shadow p-4 mt-1 rounded mb-4" />
                </div>
            </div>
        </div>
    );
};

export default ReservationForm;
