import { NavLink, Link } from "react-router-dom";
import './Site.css';

function Nav() {
  return (
    <>
      <nav className="topnav">
        <div className="container-fluid">
          <ul className="topnav-button-8 me-auto mb-2 mb-lg-0">
            <li>
            <NavLink className="navbar-brand topnav-button-8 mt-1" to="/">
              <img src="/images/MTJJAY.png" style={{ width: "75px" }} />
            </NavLink>
            </li>
            <li>
            <NavLink className="nav-link topnav-button-8 mt-5" aria-current="page" to="/">MT JJAY</NavLink>
            </li>
          </ul>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <ul className="topnav-right topnav-button-8 me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link topnav-button-8" to="stay">Reservations</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link topnav-button-8" to="food">Dining</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link topnav-button-8" to="game">Gaming</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link topnav-button-8" to="snowboard">Snowboarding</NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </>
  );
}

export default Nav;
