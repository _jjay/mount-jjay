import { Link } from "react-router-dom";
import './Site.css';

function MainPage() {

    return (
        <>
            <div className="container">
                <div className="row mt-3">
                    <div className="col-lg col-md-12 mb-4 mb-lg-0 mt-5">
                        <Link to="/food">
                            <video
                                src="/videos/garden.mp4"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="lift"
                            />
                        </Link>
                        <Link to="/snowboard">
                            <img
                                src="/images/lift.jpg"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="engine that could"
                            />
                        </Link>
                    </div>

                    <div className="col-lg mb-4 mb-lg-0">
                        <Link to="/game">
                            <video
                                src="/videos/games.mp4"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="exhaust"
                            />
                        </Link>
                        <Link to="/snowboard">
                            <img
                                src="/images/snowboarder.jpg"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="hard hat"
                            />
                        </Link>

                    </div>

                    <div className="col-lg mb-4 mb-lg-0">
                        <Link to="/stay">
                            <img
                                src="/images/room.jpg"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="hard hat"
                            />
                        </Link>
                        <Link to="/food">
                            <video
                                src="/videos/bread.mp4"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="exhaust"
                            />
                        </Link>
                    </div>

                    <div className="col-lg mb-4 mb-lg-0 mt-5">
                        <Link to="/stay">
                            <img
                                src="/images/snowresort.jpg"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="blinker fluid"
                            />
                        </Link>
                        <Link to="/game">
                            <video
                                src="/videos/peoplegames.mp4"
                                className="w-100 shadow-1-strong rounded mb-4"
                                alt="underneath"
                            />
                        </Link>
                    </div>
                </div>
            </div>
        </>
    )
}
export default MainPage;
