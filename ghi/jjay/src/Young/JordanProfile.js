
function JordanProfile() {
    return (
        <div className="game-main">
        <h1 className="jordan-title">Jordan's Profile</h1>
        <p className="game-text-one">
            LISSEN!! DIS JORDAN PAGE
        </p>
        <h3 className="game-text-three">Hobby: </h3>
            <p className="hobby-text-two">GARDENING</p>
        <h3 className="game-text-three">Favorite Food: </h3>
            <img
                className="taco"
                src="https://img.icons8.com/bubbles/512/taco.png"
                alt="Creds to https://img.icons8.com/bubbles/512/taco.png"
                style={{ width: "250px", height: "250px"}}
            />
        <h3 className="game-text-three">Favorite Color: </h3>
            <p className="favorite-color purple"></p>
            <div>
                <img
                    className="jordan-img"
                    src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-software-developer-professions-woman-diversity-flaticons-lineal-color-flat-icons.png"
                    alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-software-developer-professions-woman-diversity-flaticons-lineal-color-flat-icons.png"
                />
            </div>
        </div>
    )
}

export default JordanProfile;
