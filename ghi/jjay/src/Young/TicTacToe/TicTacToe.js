import React, { useState } from "react";
import Square from "./Square";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const TicTacToe = () => {
    const [board, setBoard] = useState(Array(9).fill(null));
    const [player, setPlayer] = useState('X');
    const [gameOver, setGameOver] = useState(false);
    const [checkHistory, setCheckHistory] = useState([Array(9).fill(null)]);

    const handleClick = (index) => {
        if (board[index] || gameOver) {
            return;
        }
        let newBoard = [...board];
        newBoard[index] = player;
        setBoard(newBoard);

        let winner = setWinner(newBoard);
        if (winner) {
            setGameOver(true);
            toast(`${winner} has won the OX game!`, {
                style: {
                    background: 'linear-gradient(#3f87a6, #ebf8e1, #f69d3c)',
                    color: 'black',
                    textAlign: 'center',
                    fontFamily: "'Little Toy Builders', sans-serif",
                    fontSize: '15px',
                    textTransform: 'uppercase',
                    letterSpacing: '2px',
                }
            });
        } else if (!newBoard.includes(null)) {
            setGameOver(true);
            toast(`OX Game Draw!`, {
                style: {
                    background: 'linear-gradient(#3f87a6, #ebf8e1, #f69d3c)',
                    color: 'black',
                    textAlign: 'center',
                    fontFamily: "'Little Toy Builders', sans-serif",
                    fontSize: '15px',
                    textTransform: 'uppercase',
                    letterSpacing: '2px',
                }
            })
        } else {
            setPlayer(player === 'X' ? 'O' : 'X');
        }
        setCheckHistory([...checkHistory, newBoard]);
    }

    const setWinner = (square) => {
        const lines = [
            [0,1,2],
            [3,4,5],
            [6,7,8],
            [0,3,6],
            [1,4,7],
            [2,5,8],
            [0,4,8],
            [2,4,6]
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a,b,c] = lines[i]
            if(square[a] && square[a] === square[b] && square[a] === square[c]) {
                return square[a];
            }
        }
        return null;
    }

    const handleResetGame = () => {
        setBoard(Array(9).fill(null));
        setPlayer('X');
        setGameOver(false);
    }

    const moves = checkHistory.map((step, move) => {
        const description = move ? `Back to Move : ${move}` : 'Back to Start';
        return (
            <li className="move-btn" key={move}>
                <button onClick={() => {jumpToMove(move)}}>
                    {description}
                </button>
            </li>
        )
    })

    const jumpToMove = (step) => {
        setBoard(checkHistory[step]);
        setGameOver(false);
        setCheckHistory(checkHistory.slice(0, step + 1));
    }

    return (
        <>
            <h1 className="tic-tac-toe-title">Ticcy Taccy Toeey</h1>
            <div className="tic-tac-toe-board">
                {
                    board.map((item, index) => {
                        return <Square btnClick={handleClick} key={index} value={item} index={index} />
                    })
                }
            </div>
            <h4 className="reset-btn-container">
                <button className="reset-btn" onClick={handleResetGame}>Reset</button>
            </h4>
            <ol className="return-btn">
                {moves}
            </ol>
            <h2 className="coming-soon">More Games Coming Soon!</h2>
            <ToastContainer position="bottom-left" />
        </>
    )
}

export default TicTacToe;
