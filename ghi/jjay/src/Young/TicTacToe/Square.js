import React from "react";

const Square = ({value, btnClick, index}) => {
    return (
        <>
            <button onClick={() => {btnClick(index)}}>{value}</button>
        </>
    )
}

export default Square;
