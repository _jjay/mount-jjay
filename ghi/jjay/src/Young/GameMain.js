import { Link } from "react-router-dom";
import "./game.css"

function GameMain() {
    return (
        <div className="game-main">
        <h1 className="game-title">Young's Land</h1>
        <p className="game-text-one">
            Tis the place that will make you feel like
        </p>
        <p className="game-text-two">
            YOU'RE IN PRESCHOOL AGAIN!
        </p>
        <div className="row">
            <div>
                <div className="new-buttons-one">
                    <Link to="/game/minigames/">
                    <img
                        src="https://img.icons8.com/external-phatplus-lineal-color-phatplus/512/external-manufacturer-corporation-phatplus-lineal-color-phatplus.png" className="img-gap" style={{ width: "50px" }}
                        alt="Creds to https://img.icons8.com/external-phatplus-lineal-color-phatplus/512/external-manufacturer-corporation-phatplus-lineal-color-phatplus.png"
                    />
                    Minigames
                    </Link>
                </div>
                <div className="new-buttons-two">
                    <Link to="/game/ash/">
                    <img
                        src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-camper-camping-flaticons-lineal-color-flat-icons.png" className="img-gap" style={{ width: "50px" }}
                        alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-camper-camping-flaticons-lineal-color-flat-icons.png"
                        />
                    Ash's Profile
                    </Link>
                </div>
                <div className="new-buttons-three">
                    <Link to="/game/jackie/">
                    <img
                        src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-snowboarder-vacation-planning-skiing-and-snowboarding-flaticons-lineal-color-flat-icons.png" className="img-gap" style={{ width: "50px" }}
                        alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-snowboarder-vacation-planning-skiing-and-snowboarding-flaticons-lineal-color-flat-icons.png"
                    />
                    Jackie's Profile
                    </Link>
                </div>
                <div className="new-buttons-four">
                    <Link to="/game/jordan/">
                    <img
                        src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-chef-professions-woman-diversity-flaticons-lineal-color-flat-icons.png" className="img-gap" style={{ width: "50px" }}
                        alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-chef-professions-woman-diversity-flaticons-lineal-color-flat-icons.png"
                    />
                    Jordan's Profile
                    </Link>
                </div>
                <div className="new-buttons-five">
                    <Link to="/game/young/">
                    <img
                        src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-gamer-esport-flaticons-lineal-color-flat-icons.png" className="img-gap" style={{ width: "50px" }}
                        alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-gamer-esport-flaticons-lineal-color-flat-icons.png"
                    />
                    Young's Profile
                    </Link>
                </div>
            </div>
        </div>
            <div>
                <img
                    className="new-img"
                    src="https://img.icons8.com/external-justicon-lineal-color-justicon/512/external-mountain-landscape-justicon-lineal-color-justicon-2.png"
                    alt="Creds to https://img.icons8.com/external-justicon-lineal-color-justicon/512/external-mountain-landscape-justicon-lineal-color-justicon-2.png"
                />
            </div>
        </div>
    )
}

export default GameMain;
