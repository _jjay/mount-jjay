
function FerrisWheel() {
    return (
        <div className="wheel-container">
            <div className="ferris-wheel-stem"></div>
            <div className="wheel">
                <span className="line"></span>
                <span className="line"></span>
                <span className="line"></span>
                <span className="line"></span>
                <span className="line"></span>
                <span className="line"></span>

                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
                <div className="cabin">
                    <div className="cabin-door">.|.</div>
                </div>
            </div>
        </div>
    )
}

export default FerrisWheel;
