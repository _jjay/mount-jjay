
function AshProfile() {
    return (
        <div className="game-main">
        <h1 className="ash-title">Ash's Profile</h1>
        <p className="game-text-one">
            WHAT IS UP Y'ALL
        </p>
        <h3 className="game-text-three">Hobby: </h3>
            <p className="hobby-text-two">LISTENIN TO MUSIC</p>
        <h3 className="game-text-three">Favorite Food: </h3>
            <img
                className="pizza"
                src="https://img.icons8.com/external-konkapp-outline-color-konkapp/512/external-pizza-seafood-konkapp-outline-color-konkapp.png"
                alt="Creds to https://img.icons8.com/external-konkapp-outline-color-konkapp/512/external-pizza-seafood-konkapp-outline-color-konkapp.png"
                style={{ width: "250px", height: "250px"}}
            />
        <h3 className="game-text-three">Favorite Color: </h3>
            <p className="favorite-color green"></p>
            <div>
                <img
                    className="ash-img"
                    src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-hiking-winter-travel-flaticons-lineal-color-flat-icons.png"
                    alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-hiking-winter-travel-flaticons-lineal-color-flat-icons.png"
                />
            </div>
        </div>
    )
}

export default AshProfile
