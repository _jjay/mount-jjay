
import "./game.css"

function YoungProfile() {
    return (
        <div className="game-main">
        <h1 className="young-title">Young's Profile</h1>
        <p className="game-text-one">
            WELCOME TO MY TMI PAGE
        </p>
        <h3 className="game-text-three">Hobby: </h3>
            <p className="hobby-text-one">I LOVE TO STARE AT THE..</p>
            <p className="hobby-text-two">CEILING</p>
        <h3 className="game-text-three">Favorite Food: </h3>
            <img
                className="burger"
                src="https://img.icons8.com/bubbles/2x/hamburger.png"
                alt="Creds to https://img.icons8.com/bubbles/2x/hamburger.png"
                style={{ width: "250px", height: "250px"}}
            />
        <h3 className="game-text-three">Favorite Color: </h3>
            <p className="favorite-color yellow"></p>
        <div>
            <img
                className="young-img"
                src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-software-developer-professions-men-diversity-flaticons-lineal-color-flat-icons-2.png"
                alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-software-developer-professions-men-diversity-flaticons-lineal-color-flat-icons-2.png"
            />
        </div>
    </div>
    )
}

export default YoungProfile
