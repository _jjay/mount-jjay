
function JackieProfile() {
    return (
        <div className="game-main">
        <h1 className="jackie-title">Jackie's Profile</h1>
        <p className="game-text-one">
            OATMEAL IS HEALTHY FOR YOU!
        </p>
        <h3 className="game-text-three">Hobby: </h3>
            <p className="hobby-text-two">SNOWBOARDING</p>
        <h3 className="game-text-three">Favorite Food: </h3>
            <img
                className="ramen"
                src="https://img.icons8.com/external-bzzricon-color-omission-bzzricon-studio/512/external-noodles-chinese-new-year-bzzricon-color-omission-bzzricon-color-omission-bzzricon-studio.png"
                alt="Creds to https://img.icons8.com/external-bzzricon-color-omission-bzzricon-studio/512/external-noodles-chinese-new-year-bzzricon-color-omission-bzzricon-color-omission-bzzricon-studio.png"
                style={{ width: "250px", height: "250px"}}
            />
        <h3 className="game-text-three">Favorite Color: </h3>
            <p className="favorite-color black"></p>
            <div>
                <img
                    className="jackie-img"
                    src="https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-female-battle-royale-flaticons-lineal-color-flat-icons.png"
                    alt="Creds to https://img.icons8.com/external-flaticons-lineal-color-flat-icons/512/external-female-battle-royale-flaticons-lineal-color-flat-icons.png"
                />
            </div>
        </div>
    )
}

export default JackieProfile
