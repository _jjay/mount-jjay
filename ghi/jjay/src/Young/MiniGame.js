import TicTacToe from "./TicTacToe/TicTacToe";
import FerrisWheel from "./FerrisWheel";

function MiniGames() {
    return (
        <div className="mini-land-bg">
            <h1 className="mini-land-title">Mini Land!</h1>
            <div>
                <TicTacToe />
            </div>
            <div className="game-main-two">
                <FerrisWheel />
            </div>
        </div>
    )
}

export default MiniGames;
