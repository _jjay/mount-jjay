import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import logo from './logo.svg';
import Nav from './Nav';
import ReservationList from './stay/ReservationList';
import ReservationForm from './stay/ReservationForm';
import MainPage from './MainPage';
// Game Section
import GameMain from './Young/GameMain';
import MiniGames from './Young/MiniGame';
import AshProfile from './Young/AshProfile';
import JackieProfile from './Young/JackieProfile';
import JordanProfile from './Young/JordanProfile';
import YoungProfile from './Young/YoungProfile';
import SnowboardForm from './SnowboardForm';
import OrderList from './food/OrderList';
import OrderFood from './food/OrderFood';



function App(props) {
  return (
    <BrowserRouter>
          <Nav />
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="stay">
            <Route path="new" element={<ReservationForm />} />
            <Route path="" element={<ReservationList reservations={props.reservations} />} />
          </Route>
          <Route path="game" element={<GameMain />} />
          <Route path="game">
            <Route path="minigames" element={<MiniGames />} />
            <Route path="ash" element={<AshProfile />} />
            <Route path="jackie" element={<JackieProfile />} />
            <Route path="jordan" element={<JordanProfile />} />
            <Route path="young" element={<YoungProfile />} />
          </Route>
          <Route path="snowboard" element={<SnowboardForm />} />
          <Route path="food">
            <Route path="" element={<OrderList />} />
            <Route path="new" element={<OrderFood />} />
          </Route>
        </Routes>
    </BrowserRouter>
  );
}
export default App;
