import React, { useEffect, useState } from "react";
import "./Dining.css"

function OrderList() {

    const [orders, setOrders] = useState([])
    const [status, setStatus] = useState(0)

    const fetchOrderList = async () => {
        const orderUrl = 'http://localhost:8010/orders/'
        const fetchOrders = await fetch(orderUrl)

        if (fetchOrders.ok) {
            const orderData = await fetchOrders.json()
            setOrders(orderData.orders)
        }
    }

    const handleCancel = async (order, event) => {

        const cancelUrl = `http://localhost:8010/cancel/${order}/`
        const response = await fetch(cancelUrl, { method: "PUT" })

        if (response.ok) {
            setStatus(status + 1)
        }

    }

    const handleStart = async (order, event) => {

        const startUrl = `http://localhost:8010/start/${order}/`
        const response = await fetch(startUrl, { method: "PUT" })

        if (response.ok) {
            setStatus(status + 1)
        }

    }

    const handleSend = async (order, event) => {

        const sendUrl = `http://localhost:8010/send/${order}/`
        const response = await fetch(sendUrl, { method: "PUT" })

        if (response.ok) {
            setStatus(status + 1)
        }

    }

    const handleDeliver = async (order, event) => {

        const deliverUrl = `http://localhost:8010/deliver/${order}/`
        const response = await fetch(deliverUrl, { method: "PUT" })

        if (response.ok) {
            setStatus(status + 1)
        }

    }

    useEffect(() => {
        fetchOrderList();
    }, [status]);


    return (
        <>
            <div className="container">
                <table className="table align-middle mt-3">
                    <thead>
                        <tr>
                            <th scope="col">Room Number</th>
                            <th scope="col">Visitor Name</th>
                            <th scope="col">Phone Number</th>
                            <th scope="col">Date</th>
                            <th scope="col">Time</th>
                            <th scope="col">Food Items</th>
                            <th scope="col">Notes</th>
                            <th scope="col">Status</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {orders && orders.map(order => {
                            const date = new Date(order.date)
                            console.log(order)
                            // const items = order.food_items.map(item => <ul>{item}</ul>)
                            return (
                                <tr className={order.status === "IN PROGRESS" ? "bg-warning-subtle" : order.status === "OUT FOR DELIVERY" ? "bg-success-subtle" : null} key={order.id}>
                                    <td>{order.room_number}</td>
                                    <td>{order.visitor_name}</td>
                                    <td>{order.phone_number}</td>
                                    <td>{date.toDateString()}</td>
                                    <td>{order.time}</td>
                                    <td>{order.food_items}</td>
                                    <td>{order.notes}</td>
                                    <td>{order.status}</td>
                                    <td><button className="btn btn-danger" onClick={() => handleCancel(order.id)}>CANCEL</button></td>
                                    <td><button className="btn btn-warning" onClick={() => handleStart(order.id)}>START</button></td>
                                    <td><button className="btn btn-success" onClick={() => handleSend(order.id)}>DELIVERY</button></td>
                                    <td><button className="btn btn-secondary" onClick={() => handleDeliver(order.id)}>DELIVERED</button></td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </>
    );


}
export default OrderList;
