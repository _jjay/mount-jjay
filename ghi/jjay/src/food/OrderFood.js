import React, { useState } from "react";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function OrderFood() {

    const [visitorName, setVisitorName] = useState('');
    const [roomNumber, setRoomNumber] = useState('');
    const [foodItems, setFoodItems] = useState([]);
    const [time, setTime] = useState('');
    const [date, setDate] = useState('');
    const [notes, setNotes] = useState('');
    const [phoneNumber, setPhoneNubmer] = useState('')
    const [number, setNumber] = useState(0)

    const handleVisitorChange = (event) => {
        const value = event.target.value
        setVisitorName(value);
    }

    const handleRoomNumberChange = (event) => {
        const value = event.target.value
        setRoomNumber(value);
    }

    const handleTimeChange = (event) => {
        const value = event.target.value
        setTime(value);
    }

    const handleDateChange = (event) => {
        const value = event.target.value
        setDate(value);
    }

    const handleNotesChange = (event) => {
        const value = event.target.value
        setNotes(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value
        setPhoneNubmer(value);
    }

    function handleFoodChoice(value) {
        setNumber(number+1)
        setFoodItems(oldArray => [...oldArray, value])
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.visitor_name = visitorName;
        data.room_number = roomNumber;
        data.food_items = foodItems;
        data.time = time;
        data.date = date;
        data.notes = notes;
        data.phone_number = phoneNumber;

        const orderUrl = "http://localhost:8010/order/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(orderUrl, fetchConfig);

        if (response.ok) {
            console.log("response okay!", response)
            const order = await response.json()
            const date = new Date(data.date)
            toast(`Thanks for ordering! 🍽️ Your order will arrive on ${date.toDateString()} by ${data.time}!`)

            setVisitorName('');
            setRoomNumber('');
            setFoodItems([]);
            setTime('');
            setDate('');
            setNotes('');
            setPhoneNubmer('');
        }

    }



    return (
        <>
        <ToastContainer />
            <div className="container">
                <div className="row">
                    <div className="col">
                        <div className="container">
                            <div className="row justify-content-end">
                                <div className="col">
                                    <div className="row offset-md-6">
                                        <a onClick={() => handleFoodChoice("sushi")}><img src="https://img.icons8.com/external-flaticons-flat-flat-icons/100/null/external-sushi-world-cuisine-flaticons-flat-flat-icons.png"/></a>
                                    </div>
                                    <div className="row offset-lg-1">
                                        <a onClick={() => handleFoodChoice("taco")}><img src="https://img.icons8.com/office/100/null/taco.png"/></a>
                                    </div>
                                    <div className="row offset-md-6">
                                        <a onClick={() => handleFoodChoice("cake")}><img src="https://img.icons8.com/office/100/null/birthday-cake--v1.png"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="shadow p-4 mt-4">
                            <div className="d-flex mb-3 align-items-center justify-content-center">
                                <h1 className="text-center">Order Room Service</h1>
                                <img src="https://img.icons8.com/external-justicon-flat-justicon/50/null/external-room-service-hotel-essentials-justicon-flat-justicon-1.png" />
                            </div>
                            <form onSubmit={handleSubmit} id="order-room-service">
                                <div className="form-floating mb-3">
                                    <input onChange={handleVisitorChange} value={visitorName} placeholder="Name" required type="text" name="visitorName" id="visitorName" className="form-control" />
                                    <label htmlFor="visitorName">Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handlePhoneNumberChange} value={phoneNumber} placeholder="Phone Number" required maxLength="10" type="number" name="phoneNumber" id="phoneNumber" className="form-control" />
                                    <label htmlFor="phoneNumber">Phone Number</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleRoomNumberChange} value={roomNumber} placeholder="Room Number" required type="number" name="roomNumber" id="roomNumber" className="form-control" />
                                    <label htmlFor="roomNumber">Room Number</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleDateChange} value={date} placeholder="Date" required type="date" name="date" id="date" className="form-control" />
                                    <label htmlFor="date">Delivery Date</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={handleTimeChange} value={time} placeholder="Time" required type="time" name="time" id="time" className="form-control" />
                                    <label htmlFor="time">Delivery Time</label>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="notes">Notes About Your Order</label>
                                    <textarea onChange={handleNotesChange} value={notes} name="notes" id="notes" className="form-control" rows="4"></textarea>
                                </div>
                                <button className="btn btn-primary">Order</button>
                            </form>
                        </div>
                    </div>
                    <div className="col">
                        <div className="row">
                            <a onClick={() => handleFoodChoice("pizza")}><img src="https://img.icons8.com/external-icongeek26-flat-icongeek26/100/null/external-pizza-mexican-food-icongeek26-flat-icongeek26.png" /></a>
                        </div>
                        <div className="row offset-lg-4">
                            <a onClick={() => handleFoodChoice("fries")}><img src="https://img.icons8.com/color/100/null/kawaii-french-fries.png"/></a>
                        </div>
                        <div className="row">
                            <a onClick={() => handleFoodChoice("sandwhich")}><img src="https://img.icons8.com/office/100/null/sandwich.png"/></a>
                        </div>
                        <div className="mt-5">
                            <table className="table align-middle">
                                <thead>
                                    <tr>
                                        <th scope="col">Order</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {foodItems.map(item => {
                                        return (
                                            <tr key={Math.random()}>
                                                <td>{item}</td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )

}

export default OrderFood;
