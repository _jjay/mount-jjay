from django.urls import path
from .views import (api_list_snowboards, api_show_snowboard)

urlpatterns = [
    path('snowboards/', api_list_snowboards, name="api_list_snowboards"),
    path('snowboard/<int:id>/', api_show_snowboard, name="api_show_snowboard"),
]
