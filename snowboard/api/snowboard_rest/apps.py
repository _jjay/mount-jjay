from django.apps import AppConfig


class SnowboardRestConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "snowboard_rest"
