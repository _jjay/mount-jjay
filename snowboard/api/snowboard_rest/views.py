from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

from common.json import ModelEncoder
from .models import Snowboard

# class VisitorVODetailEncoder(ModelEncoder):
#     model = VisitorVO
#     properties = [
#         "import_href",
#         "first_name",
#         "last_name",
#         "birthday",
#         "email",
#         "boot_size",
#         "equipment_rental",
#         "board_length",
#     ]

class SnowboardListEncoder(ModelEncoder):
    model = Snowboard
    properties = [
        "id",
        "name",
        "date",
        "email",
        "boot_size",
        "board_length",
        "equipment_rental",
    ]


class SnowboardDetailEncoder(ModelEncoder):
    model = Snowboard
    properties = [
        "id",
        "name",
        "date",
        "email",
        "boot_size",
        "board_length",
        "equipment_rental",
    ]


@require_http_methods(["GET", "POST"])
def api_list_snowboards(request):
    if request.method == "GET":
        snowboards = Snowboard.objects.all()
        return JsonResponse(
            {"snowboards": snowboards},
            encoder=SnowboardListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
        except:
            return JsonResponse(
                {"message": "snowboarder doesn't exist"},
                status=400,
            )

        snowboard = Snowboard.objects.create(**content)
        return JsonResponse(
            snowboard,
            encoder=SnowboardDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_snowboard(request, id):
    if request.method == "GET":
        try:
            snowboard = Snowboard.objects.get(id=id)
            return JsonResponse(
                snowboard,
                encoder=SnowboardDetailEncoder,
                safe=False,
            )
        except Snowboard.DoesNotExist:
            return JsonResponse(
                {"message": "invalid snowboarder"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Snowboard.objects.filter(id=id).delete()
        return JsonResponse({"delete": count > 0})
    else:
        content = json.loads(request.body)
        try:
            snowboard = Snowboard.objects.get(id=id)
        except Snowboard.DoesNotExist:
            return JsonResponse(
                {"message": "invalid boarder"},
                status=400,
            )

        Snowboard.objects.filter(id=id).update(**content)

        snowboard = Snowboard.objects.get(id=id)
        return JsonResponse(
            snowboard,
            encoder=SnowboardDetailEncoder,
            safe=False,
        )
