from django.db import models
from django.urls import reverse

# class VisitorVO(models.Model):
#     import_href = models.CharField(max_length=100, unique=True, null=True)
#     first_name = models.CharField(max_length=150)
#     last_name = models.CharField(max_length=150)
#     birthday = models.DateField()
#     email = models.EmailField()
#     boot_size = models.CharField(
#         max_length = 5,
#         null = True,
#     )
#     equipment_rental = models.BooleanField(
#         null = True,
#     )
#     board_length = models.FloatField(
#         null = True,
#     )

class Snowboard(models.Model):
    name = models.CharField(max_length=100)
    date = models.DateTimeField()
    email = models.EmailField(null=True)
    boot_size_choices = [
        ("K1", "K1"),
        ("K2", "K2"),
        ("K2.5", "K2.5"),
        ("K3", "K3"),
        ("K3.5", "K3.5"),
        ("K4", "K4"),
        ("K4.5", "K4.5"),
        ("K5", "K5"),
        ("K5.5", "K5.5"),
        ("K6", "K6"),
        ("K6.5", "K6.5"),
        ("K7", "K7"),
        ("K7.5", "K7.5"),
        ("K8", "K8"),
        ("K8.5", "K8.5"),
        ("K9", "K9"),
        ("K9.5", "K9.5"),
        ("K10", "K10"),
        ("K10.5", "K10.5"),
        ("K11", "K11"),
        ("K11.5", "K11.5"),
        ("K12", "K12"),
        ("K12.5", "K12.5"),
        ("K13", "K13"),
        ("K13.5", "K13.5"),
        ("Y1", "Y1"),
        ("Y1.5", "Y1.5"),
        ("Y2", "Y2"),
        ("Y2.5", "Y2.5"),
        ("Y3", "Y3"),
        ("M3.5", "M3.5"),
        ("M4", "M4"),
        ("M4.5", "M4.5"),
        ("M5", "M5"),
        ("M5.5", "M5.5"),
        ("M6", "M6"),
        ("M6.5", "M6.5"),
        ("M7", "M7"),
        ("M7.5", "M7.5"),
        ("M8", "M8"),
        ("M8.5", "M8.5"),
        ("M9", "M9"),
        ("M9.5", "M9.5"),
        ("M10", "M10"),
        ("M10.5", "M10.5"),
        ("M11", "M11"),
        ("M11.5", "M11.5"),
        ("M12", "M12"),
        ("M12.5", "M12.5"),
        ("M13", "M13"),
        ("M13.5", "M13.5"),
        ("M14", "M14"),
    ]
    boot_size = models.CharField(
        max_length=5,
        choices=boot_size_choices,
        default="shoe size",
        null=True,
    )
    board_length_choices=[
        ('135cm', '135com'),
        ('138cm', '138cm'),
        ('142cm', '142com'),
        ('146cm', '146cm'),
        ('151cm', '151cm'),
        ('155cm', '155cm'),
        ('157cm', '157cm'),
        ('159cm', '159cm'),
        ('161cm', '161cm'),
        ('166cm', '166cm'),
        ('172cm', '172cm'),
    ]
    board_length = models.CharField(
        max_length=5,
        choices= board_length_choices,
        default = "board length",
        null=True,
    )
    equipment_choices = [
        (True, "Yes"),
        (False, "No"),
    ]
    equipment_rental = models.BooleanField(
        choices=equipment_choices,
        default=None,
        null=True,
    )


    def __str__(self):
        return self.first_name

    def get_api_url(self):
        return reverse("api_show_snowboard", kwargs={"id": self.id})

    class Meta:
        ordering = (
            "name",
            "date",
            "email",
            "boot_size",
            "board_length",
            "equipment_rental",
        )