from django.contrib import admin
from .models import Snowboard

# Register your models here.
@admin.register(Snowboard)
class SnowboardAdmin(admin.ModelAdmin):
    pass